namespace :elasticsearch_importer do
  task run: :environment do |_task, _args|
    ElasticsearchImporter.execute
  end
end