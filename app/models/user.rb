class User < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  index_name Rails.application.class.parent_name.underscore
  document_type self.name.downcase

  has_many :interests

  settings do
    mappings dynamic: false do
      indexes :id, type: 'integer', index: true
      indexes :first_name, type: :text
      indexes :last_name, type: :text, analyzer: :english
      indexes :about, type: :text, analyzer: :english
      indexes :age, type: :integer, index: true
      indexes :has_old, type: 'boolean', index: true
      indexes :interests do
        indexes :id, type: 'integer', index: true
        indexes :name, type: 'text', index: true
      end
    end
  end

  def as_elasticsearch_index(options = {})
    user_attr = {
      id: id,
      first_name: first_name,
      last_name: last_name,
      about: about,
      age: age,
      has_old: age > 49
    }

    user_attr[:interests] = interests.map do |interest|
      {
        id: interest.id,
        name: interest.name
      }
    end

    user_attr
  end
end
