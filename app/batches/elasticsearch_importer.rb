class ElasticsearchImporter
  def self.execute
    # User.__elasticsearch__.delete_index!
    # User.__elasticsearch__.create_index!
    User.__elasticsearch__.create_index! force: true

    User.includes(:interests).find_in_batches(batch_size: 100) do |users|
      bulk_index(users, User.index_name)
    end
  end

  def self.prepare_records(users)
    users.map do |user|
      {index: {_id: user.id, data: user.as_elasticsearch_index}}
    end
  end

  def self.bulk_index(users, index_name)
    res = User.__elasticsearch__.client.bulk(
      index: index_name,
      type: ::User.__elasticsearch__.document_type,
      body: prepare_records(users),
      refresh: true
    )

    if res['errors']
      e = 'Elasticsearch Importer bulk index result have errors: ' + res.to_s
      logger.error(e)
      # Bugsnag.notify(e)
    end
  end
end